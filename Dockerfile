FROM php:5.3-apache

COPY src/* /var/www/html/

RUN mv /var/www/html/php.ini-production $PHP_INI_DIR/php.ini